package jwt

import "testing"
import (
	"time"
)

type NewTokenTest struct {
	InputAlg    SigningAlgorithm
	InputSecret []byte
	InputClaims M

	ExpectedOut string
	ExpectedErr error
}

func TestNew(t *testing.T) {
	scenarios := map[string]NewTokenTest{
		"normal HS256": NewTokenTest{
			InputAlg:    HS256,
			InputSecret: []byte("banana"),
			InputClaims: M{
				"iat": time.Now().UTC().UnixNano(),
				"sub": "smeck1999@gmail.com",
				"aud": "test.example.com",
			},

			ExpectedOut: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ0ZXN0LmV4YW1wbGUuY29tIiwiaWF0IjoxNTMwNTY4NDE5MTk4NjE4MjYwLCJzdWIiOiJzbWVjazE5OTlAZ21haWwuY29tIn0.PjgcS9M8F7IWJki8mRnigTXtIaqOuCUW9sUMvv8w6f0",
			ExpectedErr: nil,
		},
		"normal HS512": NewTokenTest{
			InputAlg:    HS512,
			InputSecret: []byte("banana"),
			InputClaims: M{
				"iat": time.Now().UTC().UnixNano(),
				"sub": "smeck1999@gmail.com",
				"aud": "test.example.com",
			},

			ExpectedOut: "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ0ZXN0LmV4YW1wbGUuY29tIiwiaWF0IjoxNTMwNTY4NDE5MTk4NjE4NDg4LCJzdWIiOiJzbWVjazE5OTlAZ21haWwuY29tIn0.SkD3lRzRy9TETm1JpQg04Q6T_B9L0fcQeabRogrj5vc5rQWPNECdHcTd91SojlHTxRHnZSpatpwfX-4yIqVPBw",
			ExpectedErr: nil,
		},
	}

	for name, scene := range scenarios {
		t.Run(name, func(test *testing.T) {
			tokenStr, err := New(scene.InputAlg, scene.InputSecret, scene.InputClaims)
			if scene.ExpectedErr == err {
				test.Errorf(
					"wanted <%v> <%v> but got <%v> <%v>",
					scene.ExpectedOut, scene.ExpectedErr,
					tokenStr, err,
				)
			}
		})
	}
}

func BenchNew(b *testing.B) {
	scenarios := map[string]NewTokenTest{
		"normal HS256": NewTokenTest{
			InputAlg:    HS256,
			InputSecret: []byte("banana"),
			InputClaims: M{
				"iat": time.Now().UTC().UnixNano(),
				"sub": "smeck1999@gmail.com",
				"aud": "test.example.com",
			},
		},
		"normal HS512": NewTokenTest{
			InputAlg:    HS512,
			InputSecret: []byte("banana"),
			InputClaims: M{
				"iat": time.Now().UTC().UnixNano(),
				"sub": "smeck1999@gmail.com",
				"aud": "test.example.com",
			},
		},
	}

	for name, scene := range scenarios {
		b.Run(name, func(bench *testing.B) {
			bench.StartTimer()
			New(scene.InputAlg, scene.InputSecret, scene.InputClaims)
			bench.StopTimer()
		})
	}
}
